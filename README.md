# Blockquote
This is the repository for a final project by Caroline Zeng (gitlab.com/chzeng) and I for [Abhi Shelat's fantastic Future Of Money seminar](https://shelat.ccis.neu.edu/18f-money/). If you are considering taking this class, I hope this project is an example of the sorts of work you'll be doing. You will definitely enjoy it. 

Our initial, high level goal was to use code similarity metrics to explore code redundancy in the Ethereum network. The exact project we had in mind had already been done:
- [Analyzing Topology of Ethereum Network](https://mislove.org/publications/Ethereum-IMC.pdf)

So, to build on these results we performed similar clustering analysis to determine the entropies of the different distributions of transactions among contract clusters. 

## Results

We reduced the complexity of the data set by only considering contracts that had been called more than 10 times.

Documents are clustered together by a similarity metric dependent on the amount of shared bytecode they have. For more information, read [Mislove's Paper](https://mislove.org/publications/Ethereum-IMC.pdf)

Fun facts:
- 106644 Contracts 
- 29909 Unique Contracts
- 9297 Clusters 

This is extremely interesting: only 28% of all contracts are unique, and of those 31% are distinct enough to be clustered separately.

Entropy distribution among the clusters, ignoring clusters with just a single contract:

![](figures/Entropies.png)

Clearly, the vast majority of contracts have relatively low entropy, implying that despite frequent code reuse most of the actuall instances of code interaction occur in few contracts.

## Resources

- [Analyzing Topology of Ethereum Network](https://mislove.org/publications/Ethereum-IMC.pdf)
- [Google Cloud Services – Ethereum Blockchain Dataset](https://cloud.google.com/)
- [Ethervm.io – Fantastic Documentation of the EVM](ethervm.io)
- [How the EVM Works](https://github.com/CoinCulture/evm-tools/blob/master/analysis/guide.md)

## Tools

- Jupyter Labs/Notebook
- SQL Database
- Python

## Style Guide

The project is organized into many pipeline scripts. Each has internal documentation of its purpose. The jupyter notebooks contain more nicely formatted information about our project and method.