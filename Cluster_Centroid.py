import numpy as np 
from scipy.sparse import *
from sklearn.preprocessing import normalize
from collections import Counter
from Secrets import DBWrapper, SMSWrapper
from math import sqrt, log
from functools import reduce
from halo import Halo
from multiprocessing import Pool
import pdb
# np.ravel_multi_index((1, 1, 1, 1, 1), (256, 256, 256, 256, 256))

opcode_bytes_csv = "data/OPCode_Bytes.csv"
opcode_bytes = {}
with open(opcode_bytes_csv) as op_f:
	idx = 0
	for opcode in op_f:
		if(opcode): # opcode could be empty
			code, name, argsize = opcode.split(",")
			code = code.encode().lower()
			opcode_bytes[code] = {"idx": idx, "code": code, "name": name, "argsize": int(argsize)}
		idx += 1


db = DBWrapper(autocommit=True)
connection = db.connection
cursor = connection.cursor()
sms = SMSWrapper()
NUM_WORKERS = 4

# A csr_matrix is a NORMALIZED vector
# A Cluster is a (Number, csr_matrix)
# A Clustering is a [List-of Cluster]
# A Document is a (Address, csr_matrix)
# A Address is a String

# String -> [List-of OpCode]
# Breaks a bytecode string into its opcodes
def make_opcodes(bytestr):
	return [bytestr[i:i+2] for i in range(0, len(bytestr), 2)]

# [List-of OpCode] -> [List-of OpCode]
# Returns a list of these opcodes where all the arguments to push instructions
#    are skipped
def remove_opargs(op_codes):
	ignore_next = 0 # number of bytes to ignore
	output_codes = []
	for op_code in op_codes:
		if ignore_next > 0:
			ignore_next -= 1
			continue
		ignore_next += opcode_bytes[op_code]["argsize"]
		output_codes.append(op_code)
	return output_codes

# [List-of X] Number -> [List-of [List-of X]]
# Returns a list of ngrams generated from the input list
def ngrams(items, n):
	return list(zip(*[items[i:] for i in range(n)]))


# (Address, Bytecode) -> Document
# Construct a csr document vector
def make_document(row):
	addr, btc = row
	opc = [opcode_bytes[code]["idx"] for code in remove_opargs(make_opcodes(btc))]
	grams = Counter(ngrams(opc, 5))
	data = np.array(list(grams.values())).reshape((-1, 1)).reshape((-1,))
	rows = np.zeros((len(grams),), dtype=np.int32)
	cols = [np.ravel_multi_index(dimmed_idx, (256,)*5) for dimmed_idx in grams.keys()]
	vec = normalize(csr_matrix((data, (rows, cols)), shape=(1, 256**5)))
	return (addr, vec)


# Clustering Document -> Clustering
# Add a document to a cluster
def add_to_cluster(clustering, doc):
	pdb.set_trace()
	addr = doc[0]
	vec = doc[1]
	candidates = []
	
	def online_avg(count, going, coming):
		return normalize(((count * going) + coming) / (count + 1))
	def send_to_db(address, cluster, entropy):
		sql = "INSERT INTO clusters (address, centroid_id, entropy) VALUES (%s, %s, %s)"
		#cursor.execute(sql, (address, cluster, entropy))
	def merge_in_db(address, cluster, candidates, entropy):
		sql = "UPDATE clusters SET centroid_id = %s WHERE centroid_id in (%s)"
		#cursor.execute(sql, (cluster, ", ".join(candidates)))
		#send_to_db(address, cluster, entropy)
	for idx in range(len(clustering)):
		simm = np.dot(clustering[idx][1].data, vec.data)
		if(simm >= 0.9):
			candidates.append(idx)
	if(len(candidates) == 1):
		clustering[idx][1] = online_avg(clustering[idx][0], clustering[idx][1], vec)
		clustering[idx][0] += 1
	elif(len(candidates) == 0):
		clustering.append((1, vec))
	elif(len(candidates) > 1):
		new_idx = candidates[0]
		new_cluster = normalize(sum([clustering[c][0] * clustering[c][1] for c in candidates], vec) / 1 + sum([clustering[c][0] for c in candidates]))
		new_size = 1 + sum([clustering[c][0] for c in candidates])
		new_clustering = [clustering[i] for i in range(len(clustering)) if i not in candidates].append((new_size, new_cluster))

	return clustering

if __name__ == "__main__":

	with Halo(text="Gathering contracts from the database...", spinner="moon"):
		#cursor.execute("SELECT address, bytecode FROM contracts")
		cursor.execute("SELECT id, opcodes FROM strict_equal_clusters LIMIT 100")
		

	with Halo(text="Making the document vectors...", spinner="moon"):
		result = cursor.fetchall()
		p = Pool(NUM_WORKERS)
		all_vectors = p.map(make_document, result)

	clustering = reduce(lambda clus, doc: add_to_cluster(clus, doc), all_vectors, [])
	sms.easy_send("All contracts have been clustered.")
	pdb.set_trace()


