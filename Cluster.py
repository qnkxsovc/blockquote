from Secrets import DBWrapper, SMSWrapper
import json
import pymysql
import numpy as np 
from math import sqrt
from collections import Counter
from halo import Halo
from multiprocessing import JoinableQueue, Process, Pool
from functools import reduce 

# Plan: 
# For each contract
# Split into ngrams
#   - Split by pairs of characters
#   - For each pair, if that pair takes arguments, remove the next pairs corresponding to those arguments
#   - You are left with all opcodes in order
#   - Split these opcodes into 5 grams
#   - Convert the 5 grams into a vector 
# Clustering:
#   - For each contract
#     - For each clustered contract
#       - If this contract has a similarity greater than 90% to the clustered contract, it joins its cluster
#     - If this contract isn't similar to any other contracts, it gets its own cluster
#   a table of all transactions to their cluster

# An OpCode is a 2 Character String representing the byte in hex identifying an opcode
# An Address is a 42 digit contract address
# A DenseVector is a ([Set [List-of Number]], [Counter [List-of Number]])
# ngram indexes in this representation have n dimensions each with range len(opcode_bytes)
# The first element may be used to enumerate which indexes occur at least once and the second
# may be used to determine how frequently each index occurs
# A Cluster is a positive number representing a cluster index
# A Clustering is a {Address : {"vec": DenseVector, "cluster": Cluster}}

opcode_bytes_csv = "data/OPCode_Bytes.csv"
opcode_bytes = {}
with open(opcode_bytes_csv) as op_f:
	idx = 0
	for opcode in op_f:
		if(opcode): # opcode could be empty
			code, name, argsize = opcode.split(",")
			code = code.encode().lower()
			opcode_bytes[code] = {"idx": idx, "code": code, "name": name, "argsize": int(argsize)}
		idx += 1

db = DBWrapper(autocommit=True)
connection = db.connection
cursor = connection.cursor(cursor=pymysql.cursors.SSCursor)
sms = SMSWrapper()

CLUSTERING = {}
NUM_WORKERS = 4
NUM_CHUNKS = 100
NUM_ROWS = 106644

# String -> [List-of OpCode]
# Breaks a bytecode string into its opcodes
def make_opcodes(bytestr):
	return [bytestr[i:i+2] for i in range(0, len(bytestr), 2)]

# [List-of OpCode] -> [List-of OpCode]
# Returns a list of these opcodes where all the arguments to push instructions
#    are skipped
def remove_opargs(op_codes):
	ignore_next = 0 # number of bytes to ignore
	output_codes = []
	for op_code in op_codes:
		if ignore_next > 0:
			ignore_next -= 1
			continue
		ignore_next += opcode_bytes[op_code]["argsize"]
		output_codes.append(op_code)
	return output_codes

# [List-of X] Number -> [List-of [List-of X]]
# Returns a list of ngrams generated from the input list
def ngrams(items, n):
	return list(zip(*[items[i:] for i in range(n)]))


# [List-of [List-of OpCode]] -> DenseVector
# Converts a list of opcode ngrams to a dense representation of the contract's ngram vector
#     Each ngram is replaced with a tuple of indexes which would correspond to a specific
#     index set to one in the canonical vector along with the frequency of occurence of that
#     index's ngram
def make_vector(opcode_grams):
	counts = dict(Counter([ tuple([opcode_bytes[code]["idx"] for code in gram]) for gram in opcode_grams]))
	return (set(counts.keys()), counts)

# DenseVector DenseVector -> Number
# Computes the dot product of two dense contract vectors
# If either vector has magnitude 0, returns 0
def similarity(left_vec, right_vec):
	# DenseVector -> Number
	# Compute magnitude
	def magnitude(vec):
		return sqrt(sum([x**2 for x in vec[1].values()]))
	ml = magnitude(left_vec)
	mr = magnitude(right_vec)
	if(ml == 0 or mr == 0):
		return 0
	else:
		shared = left_vec[0] & right_vec[0]
		norm_dot_product = sum([left_vec[1][idx] * right_vec[1][idx] for idx in shared]) / ( ml * mr )
	return norm_dot_product



# A Clustering is a List of Lists of (address, DenseVector)
# Clustering Clustering -> Clustering 
def merge_clusters(cluster1, cluster2):
	not_in_c2 = []
	for outer in cluster1:
		largest_inner_cluster = max([len(x) for x in cluster2])
		doublebreak = False
		for o_con in outer:
			o_vec = o_con[1]
			inner_offset = 0
			while (inner_offset < largest_inner_cluster):
				for inner in cluster2:
					try:
						i_vec = inner[inner_offset][1]
						if(similarity(o_vec, i_vec) >= 0.9):
							inner += outer
							doublebreak = True
							break
					except IndexError:
						pass
				if doublebreak:
					break
				inner_offset += 1
			if doublebreak:
				break
		if (not doublebreak):
			not_in_c2.append(outer) # this is an intermediate list to avoid putting elements from outer into c2 and then trying to cluster with them
	return not_in_c2 + cluster2

# [List-of (address, DenseVector)] -> Clustering
# Cluster the addresses
def cluster(contracts):
	if(len(contracts) == 1):
		return [contracts]
	split = int(len(contracts) / 2)
	left = cluster(contracts[:split])
	right = cluster(contracts[split:])
	return merge_clusters(left, right)

def process_row(row):
	address, bytecode = row
	opcodes = remove_opargs(make_opcodes(bytecode[2:]))
	ng = ngrams(opcodes, 5)
	current_dv = make_vector(ng)
	return (address, current_dv)


if __name__ == "__main__":
	with Halo(text="Gathering contracts from the database...", spinner="moon"):
		#cursor.execute("SELECT address, bytecode FROM contracts")
		cursor.execute("SELECT id, opcodes FROM strict_equal_clusters")

	chunk_size = NUM_ROWS // NUM_CHUNKS

	clustered_chunks = []

	'''
	for i in range(NUM_CHUNKS + 1):
		try:
			result = cursor.fetchmany(chunk_size)
		except pymysql.err.OperationalError:
			db.new()
			cursor = db.cursor
			cursor.execute("SELECT contracts.address, contracts.bytecode FROM contracts INNER JOIN contract_counts ON contracts.address = contract_counts.address WHERE contract_counts.count > 5")
			_ = cursor.fetchmany(i * chunk_size)
			result = cursor.fetchmany(chunk_size)
		if(len(result) > 0):
			with Halo(text="Gathering rows for chunk " + str(i), spinner="moon"):
				p = Pool(NUM_WORKERS)
				all_vectors = p.map(process_row, result)
				all_vectors_split = [all_vectors[i::NUM_WORKERS] for i in range(NUM_WORKERS)]
				del all_vectors

			with Halo(text="Clustering values for chunk " + str(i), spinner="moon"):
				clustered_parts = p.map(cluster, all_vectors_split)
				clustered_chunks.append(reduce(lambda c1, c2: merge_clusters(c1, c2), clustered_parts, []))

	with Halo(text="Working on the final merge", spinner="moon"):
		c = reduce(lambda c1, c2: merge_clusters(c1, c2), clustered_chunks, [])
	'''

	result = cursor.fetchall()
	p = Pool(NUM_WORKERS)
	all_vectors = p.map(process_row, result)
	all_vectors_split = [all_vectors[i::NUM_WORKERS] for i in range(NUM_WORKERS)]
	del all_vectors
	with Halo(text="Clustering values", spinner="moon"):
		clustered_parts = p.map(cluster, all_vectors_split)
		c = reduce(lambda c1, c2: merge_clusters(c1, c2), clustered_parts, [])

	sms.easy_send("All contracts have been clustered. Writing to the database...")
	with Halo(text="Writing the clusters to the database...", spinner="moon"):
		for idx, members in enumerate(c):
			for address, _ in members:
				sql = "INSERT INTO clusters (cluster, strict_equal_id) VALUES (%s, %s)"
				cursor.execute(sql, (idx, address))

