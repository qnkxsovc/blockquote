import json
import pymysql
import os
from Secrets import DBWrapper

workdir = "data/contracts/"

db = DBWrapper()
connection = db.connection
cursor = db.cursor

# A Smart Contract is a dictionary with the following keys:
# - address: the address of the smart contract
# - bytecode: the bytecode of the address 
# - function_sighashes: array of the signature hashes of the functions in the contract (?)
# - is_erc20: does this token follow the erc20 token standard?
# - is_erc721: does this token follow the erc721 token standard?
# - block_timestamp: timestamp of the block
# - block_number: number of the contract's block
# - block_hash: hash of the contract's block

# String -> Array
# Load a JSON file into an array of smart contracts
def loadFile(name):
	f_in = open(name)
	try:
		info = [json.loads(x) for x in f_in.read().split("\n") if x]
		f_in.close()
		return info
	except UnicodeDecodeError:
		print("Decode failure in file " + name)

# SmartContract -> _
# Take a smart contract and write it to the database
def writeContract(info):
	addr = info["address"]
	bytecode = info["bytecode"]
	hashes = json.dumps(info["function_sighashes"])
	is_erc20 = info["is_erc20"]
	is_erc721 = info["is_erc721"]
	bts = info["block_timestamp"]
	bn = int(info["block_number"])
	bh = info["block_hash"]

	statement = "INSERT INTO contracts VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
	cursor.execute(statement, (addr, bytecode, hashes, is_erc20, is_erc721, bts, bn, bh)) # note that no commit will occur

files = [os.path.join(workdir, f) for f in os.listdir(workdir) if f.endswith(".json")]

count = 0
for file in files:
	for contract in loadFile(file):
		writeContract(contract)
		count += 1
		if(count % 10000 == 0):
			print("\rAdded {0} rows".format(count), end="")
			connection.commit()
	connection.commit()
