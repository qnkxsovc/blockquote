import json
import pymysql
import os
from Secrets import DBWrapper

db = DBWrapper()
connection = db.connection
cursor = db.cursor

data_dir = "data/contract_calls"

for fname in os.listdir(data_dir):
	if(fname.endswith(".json")):
		f_in = open(os.path.join(data_dir, fname))
		for line in f_in:
			data = json.loads(line)
			sql = "UPDATE contracts SET callcount = %s WHERE address = %s"
			cursor.execute(sql, (data["f0_"], data["to_address"]))
		connection.commit()
		f_in.close()
