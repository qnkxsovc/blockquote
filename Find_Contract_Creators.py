import os
import json
import pymysql
import datetime
import itertools
from multiprocessing import Queue, Process
from collections import defaultdict
from Secrets import DBWrapper, SMSWrapper


# Make set of all known smart contract addresses
# If a transaction's recipient is in that set, add that transaction to the database
# Modelling a many-to-one relationship


# Plan:
# Make a generator of file names of smart contracts
# Keep a set of seen transaction ids
# For each name, open the file
# For each smart contract, add() the transaction id to set of seen transactions
# Make a generator of the file names of transactions
# Open a queue
# Multiprocessing function: using the (constant) set of smart contracts
#	- If the transaction's receiver is in the set, add that transaction to the queue
# Meanwhile, on a separate thread, take elements from the queue and map them to the database with their receiver as an fk 

db = DBWrapper(autocommit=True)
connection = db.connection
cursor = db.cursor

sms = SMSWrapper()

contracts_dir = "data/contracts/" # Directory containing the bulk contract data files
transactions_dir = "data/transactions/" # Directory containing the bulk transaction data files

# What contracts have I seen so far?
SEEN_CONTRACTS = set()

# Number of Transaction Processing workers. This program will spawn n+1 processes, with the extra
#    dedicated to sending database requests
NUM_WORKERS = 3
# Number -> _
# Sends status messages to sms and to the console
def easy_log(info, fmt="{0} Transactions Processed"):
	logstr = fmt.format((info))
	print(logstr)
	sms.easy_send(logstr)

# String -> [Iterator String]
# Given a directory, return a generator of all the JSON data files in the directory
def enum_data(p):
	with os.scandir(p) as files:
		for f in files:
			if(f.name.endswith(".json")):
				yield f.name

# Set Queue Queue -> _
# Processes one bulk transactions file taken from working_q, adding all transactions sent to a contract in seen to output_q
def process_transactions(seen, working_q, output_q):
	# Queue operations are synchronous within the process
	while(not working_q.empty()):
		bulk_trans = working_q.get(block=True)
		with open(os.path.join(transactions_dir, bulk_trans)) as f_trans:
			for trans_data in f_trans:
				trans = json.loads(trans_data)
				try: 
					# Is this a transaction to a known contract?
					if(trans["receipt_contract_address"] in seen):
						output_q.put(trans, block=True)
				except KeyError:
					pass
		os.rename(os.path.join(transactions_dir, bulk_trans), os.path.join(transactions_dir, bulk_trans) + ".seen")

# Queue Queue -> _
# Write items from output_q to the database as long as output_q and working_q are not empty
def write_to_db(working_q, output_q):
	# Queue operations are synchronous within the process
	count = 0
	while(not (working_q.empty() and output_q.empty())):
		trans = defaultdict(str, output_q.get(block=True))
		
		hsh = trans["hash"]
		nonce = int(trans["nonce"])
		transaction_index = int(trans["transaction_index"])
		value = trans["value"]
		gas = int(trans["gas"])
		gas_price = int(trans["gas_price"])
		inp = trans["input"]
		cumul_gas_used = int(trans["receipt_cumulative_gas_used"])
		gas_used = int(trans["receipt_gas_used"])
		root = trans["receipt_root"]
		block_timestamp = trans["block_timestamp"]
		block_number = int(trans["block_number"])
		block_hash = trans["block_hash"]
		contract_addr = trans["receipt_contract_address"]


		sql = "INSERT IGNORE INTO contract_creators VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
		cursor.execute(sql, (hsh, nonce, transaction_index, value, gas, gas_price, inp, cumul_gas_used, gas_used, root, block_timestamp, block_number, block_hash, contract_addr))
		if(count % 100000 == 0):
			easy_log("[{0}] {1}".format(datetime.datetime.now(), count))
		count += 1
	easy_log("Finished: " + str(count))

# _ -> _
# Wraps the process of grouping all seen contracts
def get_all_contracts(): 
	easy_log(datetime.datetime.now(), "[{0}] Getting all the contracts...")
	# Iterate through all the bulk contract files and create a list of all known contracts
	for bulk_contracts in enum_data(contracts_dir):
		with open(os.path.join(contracts_dir, bulk_contracts)) as f_contracts:
			for contract_data in f_contracts:
				contract = json.loads(contract_data)
				address = contract["address"]
				SEEN_CONTRACTS.add(address)
	easy_log(datetime.datetime.now(), "[{0}] All the contracts have been loaded.")

# Fill working_q with the names of all the files in transactions_dir
# Open a few processes running process_transactions on working_q
# Open one process running write_to_db 

# _ -> _
# Start the workers
def start_workers():
	working_q = Queue()
	output_q = Queue()
	for item in enum_data(transactions_dir):
		working_q.put(item)
	workers = [Process(target=process_transactions, args=(SEEN_CONTRACTS, working_q, output_q)) for _ in range(NUM_WORKERS)]
	workers.append(Process(target=write_to_db, args=(working_q, output_q)))
	for worker in workers:
		worker.start()
	easy_log(datetime.datetime.now(), "[{0}] Finished.")

if __name__ == "__main__":
	get_all_contracts()
	start_workers()
