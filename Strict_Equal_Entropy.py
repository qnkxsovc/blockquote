from Secrets import DBWrapper, SMSWrapper
from math import log
import json

# To calculate the entropy of a cluster:
# S = - sum(P_i * ln(P_i))
# Get a list of all contract counts for the contracts in this cluster
# Divide every item in the list by the sum of the list
# Calculate the sum of (lambda x : x * ln(x))
# Negate that value and return it

db = DBWrapper(autocommit=True)
connection = db.connection
cursor = db.cursor

# A Cluster is a [List-of Address]

# Cluster Cluster_ID -> Number
# Calculates the entropy of this cluster
def entropy(c, c_id):
	contracts = "('" + "', '".join(c) + "')"
	sql = "SELECT contract_counts.count FROM contract_counts WHERE address IN " + contracts
	cursor.execute(sql)
	result = cursor.fetchall()
	
	res_sum = sum([x[0] for x in result])
	norm_dist = [x[0] / res_sum for x in result]
	entropy = -1 * sum([x * log(x) for x in norm_dist])
	sql = "UPDATE strict_equal_clusters SET entropy = %s WHERE id = %s"
	cursor.execute(sql, (entropy, c_id))

cursor.execute("SELECT id, addresses FROM strict_equal_clusters")
all_clusters = cursor.fetchall()

for cluster in all_clusters:
	c_id = cluster[0]
	addresses = json.loads(cluster[1])
	entropy(addresses, c_id)