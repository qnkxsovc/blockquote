import numpy as np 
from scipy.sparse import *
from sklearn.preprocessing import normalize
from sklearn.metrics.pairwise import cosine_similarity
from collections import Counter
from Secrets import DBWrapper, SMSWrapper
from math import sqrt, log
from functools import reduce
from halo import Halo
from multiprocessing import Pool
import pdb
# np.ravel_multi_index((1, 1, 1, 1, 1), (256, 256, 256, 256, 256))

opcode_bytes_csv = "data/OPCode_Bytes.csv"
opcode_bytes = {}
with open(opcode_bytes_csv) as op_f:
	idx = 0
	for opcode in op_f:
		if(opcode): # opcode could be empty
			code, name, argsize = opcode.split(",")
			code = code.encode().lower()
			opcode_bytes[code] = {"idx": idx, "code": code, "name": name, "argsize": int(argsize)}
		idx += 1


db = DBWrapper(autocommit=True)
connection = db.connection
cursor = connection.cursor()
sms = SMSWrapper()
NUM_WORKERS = 5
# A csr_matrix is a NORMALIZED vector
# A Cluster is a (Number, csr_matrix)
# A Clustering is a [List-of Cluster]
# A Document is a (Address, csr_matrix)
# A Address is a String


# Counter Number -> Counter
# Multiplies the counts of items in a dictionary
def scale_counter(c, n):
	return Counter({key: n*value for key, value in c.items()})

# String -> [List-of OpCode]
# Breaks a bytecode string into its opcodes
def make_opcodes(bytestr):
	return [bytestr[i:i+2] for i in range(0, len(bytestr), 2)]

# [List-of OpCode] -> [List-of OpCode]
# Returns a list of these opcodes where all the arguments to push instructions
#    are skipped
def remove_opargs(op_codes):
	ignore_next = 0 # number of bytes to ignore
	output_codes = []
	for op_code in op_codes:
		if ignore_next > 0:
			ignore_next -= 1
			continue
		ignore_next += opcode_bytes[op_code]["argsize"]
		output_codes.append(op_code)
	return output_codes

# [List-of X] Number -> [List-of [List-of X]]
# Returns a list of ngrams generated from the input list
def ngrams(items, n):
	return list(zip(*[items[i:] for i in range(n)]))


# (Address, Bytecode) -> Document
# Construct a csr document vector
def make_document(row):
	addr, btc = row
	return (addr, make_vector(ngrams(remove_opargs(make_opcodes(btc)), 5)))

# DenseVector DenseVector -> Number
# Computes the dot product of two dense contract vectors
# If either vector has magnitude 0, returns 0
def similarity(left_vec, right_vec):
	# DenseVector -> Number
	# Compute magnitude
	def magnitude(vec):
		return sqrt(sum([x**2 for x in vec[1].values()]))
	ml = magnitude(left_vec)
	mr = magnitude(right_vec)
	if(ml == 0 or mr == 0):
		return 0
	else:
		shared = left_vec[0] & right_vec[0]
		norm_dot_product = sum([left_vec[1][idx] * right_vec[1][idx] for idx in shared]) / ( ml * mr )
	return norm_dot_product

# [List-of [List-of OpCode]] -> DenseVector
# Converts a list of opcode ngrams to a dense representation of the contract's ngram vector
#     Each ngram is replaced with a tuple of indexes which would correspond to a specific
#     index set to one in the canonical vector along with the frequency of occurence of that
#     index's ngram
def make_vector(opcode_grams):
	counts = Counter([ tuple([opcode_bytes[code]["idx"] for code in gram]) for gram in opcode_grams])
	return [set(counts.keys()), counts]

def in_cluster_huh(cvec, idx, vec):
		if cvec:
			simm = similarity(cvec, vec)
			if(simm >= 0.9):
				return idx

# Clustering Document -> Clustering
# Add a document to a cluster
def add_to_cluster(clustering, doc):
	WORKERS = Pool(NUM_WORKERS)
	addr = doc[0]
	vec = doc[1]
	candidates = []
	
	def online_avg(count, going, coming):
		new_count = count + 1
		new_members = going[0] | coming[0]
		combined_counters = scale_counter(going[1], count) + coming[1]
		return (new_members, scale_counter(combined_counters, 1 / new_count))

	def send_to_db(address, cluster):
		sql = "INSERT INTO se_metaclusters (address, centroid_id) VALUES (%s, %s)"
		cursor.execute(sql, (address, cluster))
	def merge_in_db(address, cluster, candidates):
		sql = "UPDATE se_metaclusters SET centroid_id = %s WHERE centroid_id in (" + ", ".join([str(x) for x in candidates]) + ")"
		cursor.execute(sql, (cluster,))
		send_to_db(address, cluster)
	

	sm_arguments = [(clustering[idx][1], idx, vec) for idx in range(len(clustering))]
	candidates = [x for x in WORKERS.starmap(in_cluster_huh, sm_arguments) if x is not None]
	WORKERS.close()

	if(len(candidates) == 1):
		send_to_db(addr, candidates[0])
		idx = candidates[0]
		clustering[idx][1] = online_avg(clustering[idx][0], clustering[idx][1], vec)
		clustering[idx][0] += 1
	elif(len(candidates) == 0):
		send_to_db(addr, len(clustering))
		clustering.append([1, vec])
	elif(len(candidates) > 1):
		new_idx = candidates[0]
		combined_counters = sum([scale_counter(clustering[c][1][1], clustering[c][0]) for c in candidates], vec[1])
		new_size = 1 + sum([clustering[c][0] for c in candidates])
		rescale = scale_counter(combined_counters, 1 / new_size)
		keys = reduce(lambda x, y: x | clustering[y][1][0], candidates, set())
		new_cluster = (keys, rescale)
		new_clustering = [clustering[i] if i not in candidates else (0, False) for i in range(len(clustering))].append(new_cluster)
		
		merge_in_db(addr, new_idx, candidates)

	return clustering

if __name__ == "__main__":
	with Halo(text="Gathering contracts from the database...", spinner="moon"):
		#cursor.execute("SELECT address, bytecode FROM contracts")
		cursor.execute("SELECT id, opcodes FROM strict_equal_clusters")


	with Halo(text="Making the document vectors...", spinner="moon"):
		result = cursor.fetchall()
		WORKERS = Pool(NUM_WORKERS)
		all_vectors = WORKERS.map(make_document, result)
		WORKERS.close()

	clustering = []
	for doc in all_vectors:
		clustering = add_to_cluster(clustering, doc)
	sms.easy_send("All contracts have been clustered.")



