#from Secrets import DBWrapper
from collections import defaultdict
from math import log
import pdb

'''
db = DBWrapper()
connection = db.connection
cursor = db.cursor
'''

import pymysql
connection = pymysql.connect(host='localhost', port=3306, user='root', password='Y@i8%i@i!~!', db='bkq')
cursor = connection.cursor()

# Calculating the entropy of document metaclusters in the ethereum contract dataset

# Goal: for every address with 10 or more incoming transactions, retreieve the cluster it belongs to
# Note the use of an optimization step: all contracts were preclustered into groups having exactly equal bytecode 
# This makes the retreival query slightly more complicated
# Then, iterate through every result and build a dictionary {cluster_id: list_of_transaction_counts }
# Then, iterate across the dictionary items to build a new dictionary {cluster_id: entropy}
# Finally, for each cluster in the database, report that cluster's entropy

sql = "SELECT se_metaclusters.`centroid_id`, contracts.callcount FROM contracts INNER JOIN se_metaclusters ON contracts.`se_cluster_id` = se_metaclusters.se_id"
cursor.execute(sql)

cluster_counts = defaultdict(list)
for item in cursor.fetchall():
	cluster_counts[item[0]].append(item[1])

cluster_entropies = {}
for cluster_id, incoming_transactions in cluster_counts.items():
	def entropy(nums):
		total = sum(nums)
		return -1 * sum([(x / total) * log(x / total) for x in nums])
	cluster_entropies[cluster_id] = entropy(incoming_transactions)

for cluster_id, entropy in cluster_entropies.items():
	sql = "UPDATE contract_clusters SET entropy = %s WHERE id = %s"
	cursor.execute(sql, (entropy, cluster_id))
connection.commit()