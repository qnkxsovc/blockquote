# Make a list of all known contracts
import Preprocess_Transactions
import json

data_dir = "data/"
Preprocess_Transactions.get_all_contracts()
f_out = open(data_dir + "All_Contracts.json", "w+")
f_out.write(json.dumps(list(Preprocess_Transactions.SEEN_CONTRACTS)))
f_out.close()
